#include <stdio.h>
int input()
{
    int num;
    printf("Enter the number\n");
    scanf("%d",&num);
    return num;
}
int compute(int num)
{
    int rev=0,rem;
    while(num>0)
    {
        rem=num%10;
        rev=rev*10+rem;
        num=num/10;
    }
    return rev;
}
void output(int num,int rev)
{
    if(num==rev)
        printf("It is a palindrome\n");
    else
        printf("Its not a palindrome\n");
}
int main()
{
  int num,rev;
  num=input();
  rev=compute(num);
  output(num,rev);
  return 0;
}
